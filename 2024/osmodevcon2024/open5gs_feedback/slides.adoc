open5gs - feedback from user point of view
==========================================
:author:	Alexander Couzens <acouzens@sysmocom.de>, Oliver Smith <osmith@sysmocom.de>, Pau Espin Pedrol <pespin@sysmocom.de>
:copyright:	2024 by sysmocom - s.f.m.c. GmbH <info@sysmocom.de> (License: CC-BY-SA)
:backend:	slidy
:max-width:	45em

== open5gs - feedback from user point of view

* Source file structure:
** (*-build.c, *-path.c, *-handler.c, *-context.c, *-sm.c)

== open5gs - feedback from user point of view

* Enc/dec code generated automatically from .docx spec files using python scripts

== open5gs - feedback from user point of view

* Split git repository vs unique git repository

== open5gs - feedback from user point of view

* Commits:
** Meaningful commit messages
** Logical change per commit

== open5gs - feedback from user point of view

* `assert()`

== open5gs - feedback from user point of view

* Config file syntax backward compatibility

== open5gs - feedback from user point of view

* mongodb backend:
** DB requirements?
** Alternatives?
